package ht.mbds.applef.datasources;

import java.util.List;

import ht.mbds.applef.datasources.models.AuteurModel;
import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.util.BankUtil;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AuteurService {

    @GET("authors")
    Call<List<AuteurModel>> getAuteurs(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization);

    @GET("authors/{id}")
    Call<AuteurModel> getAuteur(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id);

    @POST("auteur")
    Call<AuteurModel> ajouterAuteur(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Body AuteurModel auteurModel);

    @PUT("auteur/{id}")
    Call<AuteurModel> modifierAuteur(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id, @Body AuteurModel auteurModel);
}
