package ht.mbds.applef.datasources;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.AuteurModel;
import ht.mbds.applef.util.BankUtil;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public class AuteurDataSource {

    AuteurService service;


    public AuteurDataSource() {
        // Interceteur permettant de logger le traffic HTTP lors des appels au web service
        // Très utile pour débuguer, il permet de voir les requêtes et réponses du web service
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        // ici on met le niveau à body ==> on affiche tout (requête, réponse, headers, ...)
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Client HTTP utilisé pour effectuer les requêtes
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        // Ajouter un intercepteur sur toutes les requêtes afin de rejouter automatiquement l'API Key
        // Qui est obligatoire pour appeler l'API

       /* httpClient.interceptors().add(chain -> {
            Request request = chain.request();
            Request newRequest;
            newRequest = request.newBuilder()
                    .addHeader(BankUtil.NAME_HEADER_TOKEN, MainActivity.KEY_ACCESS)
                    .build();
            return chain.proceed(newRequest);

            *//*Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter(BankUtil.NAME_HEADER_TOKEN, "786a348d1d0c462db93a014b4793683b")
                    .build();
            Request.Builder requestBuilder = original.newBuilder()
                    .url(url);
            Request request = requestBuilder.build();
            return chain.proceed(request);*//*
        });*/

        // Instance de retrofit pour la gestion des requêtes vers le web service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BankUtil.URL_BASE_API) // Url de base du web service
                .client(httpClient) // Client HTTP
                .addConverterFactory(GsonConverterFactory.create()) // Converter JSON --> Objet Web service
                .build();
        // On utilise retrofit pour instancier le service
        service = retrofit.create(AuteurService.class);
    }



   public List<AuteurModel> getAuteurs() throws IOException {
        return service.getAuteurs(MainActivity.KEY_ACCESS).execute().body();
    }


    public AuteurModel getAuteur(int id)  throws IOException{
        return service.getAuteur(MainActivity.KEY_ACCESS, id).execute().body();
    }


    public AuteurModel ajouterAuteur( AuteurModel auteurModel) throws IOException {
        return service.ajouterAuteur(MainActivity.KEY_ACCESS, auteurModel).execute().body();
    }


    public AuteurModel modifierAuteur(AuteurModel auteurModel) throws IOException {
        return service.modifierAuteur(MainActivity.KEY_ACCESS,auteurModel.getAuthorId(), auteurModel).execute().body();
    }


}
