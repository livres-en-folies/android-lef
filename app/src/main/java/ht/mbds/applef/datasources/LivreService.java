package ht.mbds.applef.datasources;

import java.util.List;

import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.util.BankUtil;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface LivreService {


    @GET("books")
    Call<List<LivreModel>> getLivres(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization);

    @GET("books/{id}")
    Call<LivreModel> getLivre(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id);

    @POST("books")
    Call<LivreModel> ajouterLivre(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Body LivreModel livreModel);

    @PUT("books/{id}")
    Call<LivreModel> modifierLivre(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id, @Body LivreModel livreModel);


}

