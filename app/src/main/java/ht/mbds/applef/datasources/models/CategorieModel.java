package ht.mbds.applef.datasources.models;

public class CategorieModel {

    private int genreId ;
    private String name ;


    public CategorieModel() {
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
