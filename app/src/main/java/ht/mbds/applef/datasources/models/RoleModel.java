package ht.mbds.applef.datasources.models;

public class RoleModel {

    private int id ;
    private String name ;

    public RoleModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
