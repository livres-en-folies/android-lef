package ht.mbds.applef.datasources.models;


public class LivreModel {
    private int bookId;
    private String isbn;
    private String title;
    private String summary;
    private String edition;
    private String publisher;
    private int year;
    private double price;
    private String image;
    private int authorId;
    private int genreId;
    private String currency="HTG";
    private AuteurModel author;
    private CategorieModel genre;


    public LivreModel(String isbn, String title, String summary, String edition, String publisher, int year, double price,  String image) {
        this.isbn = isbn;
        this.title = title;
        this.summary = summary;
        this.edition = edition;
        this.publisher = publisher;
        this.year = year;
        this.price = price;
        this.image = image;
    }

    public LivreModel() {

    }

    public LivreModel(int bookId, String isbn, String title, String summary, String edition, String publisher, int year, double price, String image, int authorId, int genreId) {
        this.bookId = bookId;
        this.isbn = isbn;
        this.title = title;
        this.summary = summary;
        this.edition = edition;
        this.publisher = publisher;
        this.year = year;
        this.price = price;
        this.image = image;
        this.authorId = authorId;
        this.genreId = genreId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public AuteurModel getAuthor() {
        return author;
    }

    public void setAuthor(AuteurModel author) {
        this.author = author;
    }

    public CategorieModel getGenre() {
        return genre;
    }

    public void setGenre(CategorieModel genre) {
        this.genre = genre;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
