package ht.mbds.applef.datasources;

import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.datasources.models.UserModel;
import ht.mbds.applef.dto.DataConnexion;
import ht.mbds.applef.dto.ResponseRegister;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    @POST("auth/signin")
    Call<UserModel> connexion(@Body DataConnexion dt);

    @POST("auth/signup")
    Call<ResponseRegister> register(@Body UserModel dt);
}
