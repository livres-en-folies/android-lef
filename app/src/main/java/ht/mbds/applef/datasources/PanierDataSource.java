package ht.mbds.applef.datasources;


import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.PanierModel;
import ht.mbds.applef.dto.EnvoyerPanier;
import ht.mbds.applef.util.BankUtil;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public class PanierDataSource {
    PanierService service;

    public PanierDataSource() {

        // Interceteur permettant de logger le traffic HTTP lors des appels au web service
        // Très utile pour débuguer, il permet de voir les requêtes et réponses du web service
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        // ici on met le niveau à body ==> on affiche tout (requête, réponse, headers, ...)
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Client HTTP utilisé pour effectuer les requêtes
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        // Instance de retrofit pour la gestion des requêtes vers le web service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BankUtil.URL_BASE_API) // Url de base du web service
                .client(httpClient) // Client HTTP
                .addConverterFactory(GsonConverterFactory.create()) // Converter JSON --> Objet Web service
                .build();
        // On utilise retrofit pour instancier le service
        service = retrofit.create(PanierService.class);

    }


   public List<PanierModel> getPaniers() throws IOException {
        return service.getPaniers(MainActivity.KEY_ACCESS).execute().body();
    }

    public PanierModel getOnePanier(int id) throws IOException {

        return service.getOnePanier(MainActivity.KEY_ACCESS, id).execute().body();
    }


    public List<PanierModel> getPanierByUser(int id) throws IOException {

        return service.getPanierByUser(MainActivity.KEY_ACCESS, id).execute().body();
    }

    public PanierModel ajouterPanier( EnvoyerPanier envoyerPanier) throws IOException {

        return service.ajouterPanier(MainActivity.KEY_ACCESS, envoyerPanier).execute().body();
    }

    public PanierModel deleteOnePanier( int id) throws IOException {
       return service.deleteOnePanier(MainActivity.KEY_ACCESS, id).execute().body();
   }

    public PanierModel modifierOnePanier( int id,  EnvoyerPanier envoyerPanier) throws IOException {
        return service.modifierOnePanier(MainActivity.KEY_ACCESS, id, envoyerPanier).execute().body();
    }


}
