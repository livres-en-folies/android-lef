package ht.mbds.applef.datasources;

import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;
import ht.mbds.applef.util.BankUtil;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface InitTransactionService {

    @POST("compte/init_transaction")
    Call<ResponseDTO<ReturnInit>> initTransaction(@Body InitTransaction initT);
}
