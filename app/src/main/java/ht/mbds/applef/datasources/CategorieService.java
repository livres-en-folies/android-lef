package ht.mbds.applef.datasources;

import java.util.List;

import ht.mbds.applef.datasources.models.AuteurModel;
import ht.mbds.applef.datasources.models.CategorieModel;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.util.BankUtil;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CategorieService {

    @GET("genres")
    Call<List<CategorieModel>> getCategories(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization);

    @GET("genres/{id}")
    Call<CategorieModel> getCategorie(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization,@Path("id") int id);

    @POST("genres")
    Call<CategorieModel> ajouterCategorie(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Body CategorieModel categorieModel);

    @PUT("genres/{id}")
    Call<CategorieModel> modifierCategorie(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id, @Body CategorieModel categorieModel);
}
