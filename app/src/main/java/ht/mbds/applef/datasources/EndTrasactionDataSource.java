package ht.mbds.applef.datasources;

import java.io.IOException;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.dto.EndTransaction;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;
import ht.mbds.applef.util.BankUtil;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class EndTrasactionDataSource {

    EndTrasactionService service;

    public EndTrasactionDataSource() {

        // Interceteur permettant de logger le traffic HTTP lors des appels au web service
        // Très utile pour débuguer, il permet de voir les requêtes et réponses du web service
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        // ici on met le niveau à body ==> on affiche tout (requête, réponse, headers, ...)
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // Client HTTP utilisé pour effectuer les requêtes
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        // Instance de retrofit pour la gestion des requêtes vers le web service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BankUtil.URL_BASE_API) // Url de base du web service
                .client(httpClient) // Client HTTP
                .addConverterFactory(GsonConverterFactory.create()) // Converter JSON --> Objet Web service
                .build();
        // On utilise retrofit pour instancier le service
        service = retrofit.create(EndTrasactionService.class);
    }

    public  ResponseDTO<ReturnInit> endTransaction( EndTransaction endT) throws IOException {
        return service.endTransaction(MainActivity.KEY_ACCESS, endT.getUserId(), endT).execute().body();
    }
}
