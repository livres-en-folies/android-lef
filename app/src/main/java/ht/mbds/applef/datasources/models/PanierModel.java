package ht.mbds.applef.datasources.models;

import ht.mbds.applef.dto.Livre;

public class PanierModel {

    private int basketId;
    private int quantity ;
    private int userId ;
    private LivreModel book ;




    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LivreModel getBook() {
        return book;
    }

    public void setBook(LivreModel book) {
        this.book = book;
    }
}
