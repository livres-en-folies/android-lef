package ht.mbds.applef.datasources;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.util.BankUtil;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LivreDataSource {

    LivreService service;

    public LivreDataSource() {

        // Interceteur permettant de logger le traffic HTTP lors des appels au web service
        // Très utile pour débuguer, il permet de voir les requêtes et réponses du web service
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        // ici on met le niveau à body ==> on affiche tout (requête, réponse, headers, ...)
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Client HTTP utilisé pour effectuer les requêtes
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        // Instance de retrofit pour la gestion des requêtes vers le web service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BankUtil.URL_BASE_API) // Url de base du web service
                .client(httpClient) // Client HTTP
                .addConverterFactory(GsonConverterFactory.create()) // Converter JSON --> Objet Web service
                .build();
        // On utilise retrofit pour instancier le service
        service = retrofit.create(LivreService.class);

    }

    public List<LivreModel> getLivres() throws IOException {
        return service.getLivres(MainActivity.KEY_ACCESS).execute().body();
    }

    public LivreModel getLivre(int id) throws IOException {
        return service.getLivre(MainActivity.KEY_ACCESS, id).execute().body();
    }

    public LivreModel ajouterLivre(LivreModel livreModel) throws IOException {
        return service.ajouterLivre(MainActivity.KEY_ACCESS, livreModel).execute().body();
    }

    public LivreModel modifierLivre(LivreModel livreModel) throws IOException {
        return service.modifierLivre(MainActivity.KEY_ACCESS, livreModel.getBookId(), livreModel).execute().body();
    }
}
