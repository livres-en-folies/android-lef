package ht.mbds.applef.datasources;

import java.util.List;

import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.datasources.models.PanierModel;
import ht.mbds.applef.dto.EnvoyerPanier;
import ht.mbds.applef.util.BankUtil;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PanierService {

    @GET("baskets")
    Call<List<PanierModel>> getPaniers(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization);

    @GET("baskets/{id}")
    Call<PanierModel> getOnePanier(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id);

    @GET("baskets/user/{id}")
    Call<List<PanierModel>> getPanierByUser(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id);

    @POST("baskets")
    Call<PanierModel> ajouterPanier(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Body EnvoyerPanier envoyerPanier);

    @DELETE("baskets/{id}")
    Call<PanierModel> deleteOnePanier(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id);

    @PUT("baskets/{id}")
    Call<PanierModel> modifierOnePanier(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("id") int id, @Body EnvoyerPanier envoyerPanier);

}
