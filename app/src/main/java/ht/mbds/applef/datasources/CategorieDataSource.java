package ht.mbds.applef.datasources;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.CategorieModel;
import ht.mbds.applef.util.BankUtil;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public class CategorieDataSource {

    CategorieService service;

    public CategorieDataSource() {

        // Interceteur permettant de logger le traffic HTTP lors des appels au web service
        // Très utile pour débuguer, il permet de voir les requêtes et réponses du web service
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        // ici on met le niveau à body ==> on affiche tout (requête, réponse, headers, ...)
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Client HTTP utilisé pour effectuer les requêtes
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        // Instance de retrofit pour la gestion des requêtes vers le web service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BankUtil.URL_BASE_API) // Url de base du web service
                .client(httpClient) // Client HTTP
                .addConverterFactory(GsonConverterFactory.create()) // Converter JSON --> Objet Web service
                .build();
        // On utilise retrofit pour instancier le service
        service = retrofit.create(CategorieService.class);
    }

    public List<CategorieModel> getCategories() throws IOException {
         return  service.getCategories(MainActivity.KEY_ACCESS).execute().body();
    }

   public  CategorieModel getCategorie(int id) throws IOException {
       return  service.getCategorie(MainActivity.KEY_ACCESS, id).execute().body();
   }

    public CategorieModel ajouterCategorie(CategorieModel categorieModel) throws IOException {
        return  service.ajouterCategorie(MainActivity.KEY_ACCESS, categorieModel).execute().body();
    }


    public CategorieModel modifierCategorie( int id, CategorieModel categorieModel) throws IOException {
        return  service.modifierCategorie(MainActivity.KEY_ACCESS, id, categorieModel).execute().body();
    }

}
