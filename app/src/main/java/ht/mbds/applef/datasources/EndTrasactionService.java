package ht.mbds.applef.datasources;

import ht.mbds.applef.dto.EndTransaction;
import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;
import ht.mbds.applef.util.BankUtil;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface EndTrasactionService {

    @POST("orders/{userId}/payment")
    Call<ResponseDTO<ReturnInit>> endTransaction(@Header(BankUtil.NAME_HEADER_TOKEN) String authorization, @Path("userId") int userId, @Body EndTransaction endT);
}
