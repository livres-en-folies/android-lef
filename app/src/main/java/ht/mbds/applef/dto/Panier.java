package ht.mbds.applef.dto;

public class Panier {
    private int basketId;
    private int quantity ;
    private int userId ;
    private Livre book ;

    public Panier() {

    }

    public Panier(int basketId, int quantity, int userId, Livre book) {
        this.basketId = basketId;
        this.quantity = quantity;
        this.userId = userId;
        this.book = book;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Livre getBook() {
        return book;
    }

    public void setBook(Livre book) {
        this.book = book;
    }
}
