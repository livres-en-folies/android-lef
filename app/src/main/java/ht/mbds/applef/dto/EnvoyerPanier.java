package ht.mbds.applef.dto;

public class EnvoyerPanier {

    private int basketId;
    private int quantity;
    private int bookId;
    private int userId;
    private double total;

    public EnvoyerPanier() {

    }

    public EnvoyerPanier(int quantity, int bookId, int userId, double total) {
        this.quantity = quantity;
        this.bookId = bookId;
        this.userId = userId;
        this.total = total;
    }

    public EnvoyerPanier(int basketId, int quantity, int bookId, int userId, double total) {
        this.basketId = basketId;
        this.quantity = quantity;
        this.bookId = bookId;
        this.userId = userId;
        this.total = total;
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
