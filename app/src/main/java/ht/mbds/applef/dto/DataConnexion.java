package ht.mbds.applef.dto;

public class DataConnexion {


   private String username;
    private String password;

    public DataConnexion() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
