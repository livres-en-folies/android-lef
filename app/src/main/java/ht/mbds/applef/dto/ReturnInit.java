package ht.mbds.applef.dto;

import android.media.VolumeShaper;

public class ReturnInit {
    private Operation operation ;
    private String token ;


    public ReturnInit() {
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
