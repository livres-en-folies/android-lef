package ht.mbds.applef.util;

public class BankUtil {

    public static final String KEY_SCREEN = "ecran";
    public static final String KEY_LIVRE = "livre";
    public static final String KEY_REGISTER = "register";
    public static final String KEY_DETAIL_LIVRE = "detail-livre";
    public static final String KEY_CATEGORIE = "categorie";
    public static final String KEY_AUTEUR = "auteur";
    public static final String KEY_PAIEMENT = "paiement";
    public static final String KEY_MONTANT_PAIEMENT = "montant_paiement";
    public static final String KEY_ID = "KEY_ID";


    //public static final String URL_BASE_API = "http://192.168.0.104:4000/api/";
   // public static final String URL_BASE_API = "http://192.168.15.142:4000/api/";
    public static final String URL_BASE_API = "http://api.livresenfolies.online/api/";
    public static final String URL_BASE_PAY_API = "http://livresenfolies.online:8080/bank/webresources/";
    public static final String NAME_HEADER_TOKEN = "x-access-token";

    public static final String LIBELLE_KEY_ACCESS = "LIBELLE_KEY_ACCESS";
    public static final String DATA_CONNEXION = "dataconn";



}
