package ht.mbds.applef.ui.categorie;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.repositories.LivreRepository;

public class CUCategorieViewModel extends ViewModel {
    CategorieRepository categorieRepository = new CategorieRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<Categorie> categorie = new MutableLiveData<>();
    public LiveData<Categorie> getOneCategorie(){
        return categorie;
    }


    public void getCategorie(int id) {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                // Récupérer la liste des articles via le repository
                Categorie cat = categorieRepository.getCategorie(id);
                // Mettre à jour le live data
                categorie.postValue(cat);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}