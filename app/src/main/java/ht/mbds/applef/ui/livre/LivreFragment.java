package ht.mbds.applef.ui.livre;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.util.BankUtil;

public class LivreFragment extends Fragment implements LivreRecyclerViewAdapter.MyClickListener {

    private LivreViewModel viewModel;
    private RecyclerView mRecyclerView;
    private  FloatingActionButton fab;
    public static LivreFragment newInstance() {
        return new LivreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.livre_fragment, container, false);
        Context context = view.getContext();
        fab = view.findViewById(R.id.id_but_create_livre);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_livre);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        return  view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       // viewModel = new ViewModelProvider(this).get(LivreViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(LivreViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LivreFragment fragment = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentLivreForm = new Intent(fragment.getContext() , FormActivity.class);
                intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_LIVRE);
                intentLivreForm.putExtra(BankUtil.KEY_ID, 0);
                startActivity(intentLivreForm);
            }
        });

        if(!MainActivity.user.getRoles()[0].equalsIgnoreCase("ADMIN") && !MainActivity.user.getRoles()[0].equalsIgnoreCase("ROLE_ADMIN")){
            fab.setVisibility(View.GONE);
        }


        viewModel.getListLivre().observe(getViewLifecycleOwner(), new Observer<List<Livre>>() {
            @Override
            public void onChanged(List<Livre> livres) {
                 mRecyclerView.setAdapter(new LivreRecyclerViewAdapter(livres, fragment));
            }
        });

        viewModel.getLivres();
        viewModel.getLivres();
        viewModel.getLivres();
    }

    @Override
    public void onItemClick(Long idNeighbour) {

    }
}