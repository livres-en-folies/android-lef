package ht.mbds.applef.ui.paiement;

import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.datasources.EndTrasactionDataSource;
import ht.mbds.applef.dto.EndTransaction;
import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.Panier;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;
import ht.mbds.applef.dto.User;
import ht.mbds.applef.repositories.InitTransactionRepository;
import ht.mbds.applef.util.BankUtil;

public class PaiementFragment extends Fragment {

    private PaiementViewModel mViewModel;

    public static PaiementFragment newInstance() {
        return new PaiementFragment();
    }
    double montant = 0;

    private TextView textViewCompte;
    private TextView textViewLogin;
    private TextView textViewPassword;
    private TextView textViewid_libelle_montant;
    private Button buttonValider;
private Context context;
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();
    InitTransactionRepository initTransactionRepository = new InitTransactionRepository();
    EndTrasactionDataSource endTrasactionDataSource = new EndTrasactionDataSource();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.paiement_fragment, container, false);
        montant= getArguments().getDouble(BankUtil.KEY_MONTANT_PAIEMENT);
        textViewCompte= view.findViewById(R.id.id_compte_paiement);
        textViewLogin= view.findViewById(R.id.id_login_paiement);
        textViewPassword= view.findViewById(R.id.id_password_paiement);
        textViewid_libelle_montant= view.findViewById(R.id.id_libelle_montant);
        buttonValider=view.findViewById(R.id.createPaiement);


        context = view.getContext();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(PaiementViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textViewid_libelle_montant.setText("Le Montant a Payer est de : "+montant);
        buttonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textViewCompte.getText()==null || textViewCompte.getText().toString().equalsIgnoreCase("")){
                    textViewCompte.setError("Champs obligatoire");
                    return;
                }

                if(textViewLogin.getText()==null || textViewLogin.getText().toString().equalsIgnoreCase("")){
                    textViewLogin.setError("Champs obligatoire");
                    return;
                }

                if(textViewPassword.getText()==null || textViewPassword.getText().toString().equalsIgnoreCase("")){
                    textViewPassword.setError("Champs obligatoire");
                    return;
                }




              executor.execute(() -> {
                try {
                    InitTransaction initTransaction = new InitTransaction();
                    initTransaction.setMontant(montant);
                    initTransaction.setNumeroCompte(textViewCompte.getText().toString().trim());
                    initTransaction.setLogin(textViewLogin.getText().toString().trim());
                    initTransaction.setPassword(textViewPassword.getText().toString().trim());
                    ResponseDTO<ReturnInit> returnInitResponseDTO= initTransactionRepository.initTransaction(initTransaction);
                    myFunction(returnInitResponseDTO);
                }catch (Exception e){

                    System.out.println("GOD my life "+e.getMessage());
                }
            });


            }
        });
    }


    public void myFunction(ResponseDTO<ReturnInit> returnInitResponseDTO){
        handler.post(new Runnable() {
            @Override
            public void run() {


                executor.execute(() -> {
                    try {
                        if(returnInitResponseDTO.getCode()==200){
                             EndTransaction endTransaction = new EndTransaction();
                            endTransaction.setIdOperation(returnInitResponseDTO.getData().getOperation().getId());
                            endTransaction.setMontant(returnInitResponseDTO.getData().getOperation().getMontant());
                            endTransaction.setNumeroCompte(returnInitResponseDTO.getData().getOperation().getNumeroCompte());
                            endTransaction.setUserId(MainActivity.user.getId());
                            endTransaction.setToken(returnInitResponseDTO.getData().getToken());
                            endTrasactionDataSource.endTransaction(endTransaction);
                            myFunction2(200, "Operation reussie", returnInitResponseDTO);
                        }else{
                            myFunction2(400, returnInitResponseDTO.getMessage(), returnInitResponseDTO);
                        }

                    }catch (Exception e){
                        myFunction2(400, "Error innatendu ", null);
                        System.out.println("GOD my life "+e.getMessage());
                    }
                });


            }
        });
    }

    public void myFunction2(int code , String message , ResponseDTO<ReturnInit> returnInitResponseDTO){
        handler.post(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                getActivity().finish();

            }
        });
    }
}