package ht.mbds.applef.ui.auteur;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.R;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.ui.livre.CULivreViewModel;
import ht.mbds.applef.util.BankUtil;

public class CUAuteurFragment extends Fragment {

    private CUAuteurViewModel mViewModel;
    AuthorRepository authorRepository = new AuthorRepository();
    private int idAuthor=0;
    private Context context =null;
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();


    private EditText editTextid_author_cu ;
    private EditText editTextid_aname ;
    private EditText editTextid_aemail ;
    private EditText editTextid_aphone ;
    private EditText editTextid_awebsite ;
    private Button butt_id_create_auteur;


    public static CUAuteurFragment newInstance() {
        return new CUAuteurFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.c_u_auteur_fragment, container, false);

        idAuthor= getArguments().getInt(BankUtil.KEY_ID);
        editTextid_author_cu = view.findViewById(R.id.id_author_cu);
        editTextid_aname  = view.findViewById(R.id.id_aname);
        editTextid_aemail  = view.findViewById(R.id.id_aemail);
        editTextid_aphone  = view.findViewById(R.id.id_aphone);
        editTextid_awebsite  = view.findViewById(R.id.id_awebsite);
        butt_id_create_auteur = view.findViewById(R.id.id_create_auteur);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CUAuteurViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context= getContext();
        mViewModel = new ViewModelProvider(this).get(CUAuteurViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Recuperation Livre
        mViewModel.getOneAuthor().observe(getViewLifecycleOwner(), new Observer<Author>() {
            @Override
            public void onChanged(Author author) {
                //eid_idL.setText(String.valueOf(livre.getBookId()));
                editTextid_author_cu.setText(String.valueOf(author.getAuthorId()));
                editTextid_aname.setText(author.getName());
                editTextid_aemail.setText(author.getEmail());
                editTextid_aphone.setText(author.getPhone());
                editTextid_awebsite.setText(author.getWebsite());
            }
        });

        if(idAuthor!=0){
            System.out.println("GOD >> code autheur "+idAuthor);
            mViewModel.getAuthor(idAuthor);
        }else{
            System.out.println("GOD >> code autheur zero");
        }


        butt_id_create_auteur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             /*  if(editTextid_author_cu==null || editTextid_author_cu.getText().toString().equalsIgnoreCase("")){
                   editTextid_author_cu.setError("Champs obligatoire");
                   return;
               }*/

                if(editTextid_aname==null || editTextid_aname.getText().toString().equalsIgnoreCase("")){
                    editTextid_aname.setError("Champs obligatoire");
                    return;
                }

                if(editTextid_aemail==null || editTextid_aemail.getText().toString().equalsIgnoreCase("")){
                    editTextid_aemail.setError("Champs obligatoire");
                    return;
                }

                if(editTextid_aphone==null || editTextid_aphone.getText().toString().equalsIgnoreCase("")){
                    editTextid_aphone.setError("Champs obligatoire");
                    return;
                }

                if(editTextid_awebsite==null || editTextid_awebsite.getText().toString().equalsIgnoreCase("")){
                    editTextid_awebsite.setError("Champs obligatoire");
                    return;
                }

                executor.execute(() -> {
                    try {

                        Author author = new Author();
                        author.setAuthorId(Integer.valueOf(editTextid_author_cu.getText().toString()));
                        author.setName(editTextid_aname.getText().toString());
                        author.setEmail(editTextid_aemail.getText().toString());
                        author.setPhone(editTextid_aphone.getText().toString());
                        author.setWebsite(editTextid_awebsite.getText().toString());
                        if(idAuthor == 0){
                            authorRepository.ajouterAuteur(author);
                        }else {
                            authorRepository.modifierAuteur(author);
                        }
                        notifieMe(200, "operation reussie");

                    }catch (Exception e){
                        String msg = "error : ";
                        if(e==null || e.getMessage() == null){
                            notifieMe(400, "Error");
                        }else {
                            notifieMe(400, "Error : "+e.getMessage());
                        }
                    }
                });

            }
        });
    }


    public void notifieMe(int code , String message){
        handler.post(new Runnable() {
            @Override
            public void run() {
                    if(code==200){
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
            }
        });

    }
}