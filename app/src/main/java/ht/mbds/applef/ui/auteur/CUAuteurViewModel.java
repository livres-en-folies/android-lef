package ht.mbds.applef.ui.auteur;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.repositories.LivreRepository;

public class CUAuteurViewModel extends ViewModel {
    AuthorRepository authorRepository = new AuthorRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<Author> author= new MutableLiveData<>();

    public LiveData<Author> getOneAuthor(){
        return author;
    }


    public void getAuthor(int id) {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                Author livr = authorRepository.getAuteur(id);
                author.postValue(livr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}