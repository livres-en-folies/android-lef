package ht.mbds.applef.ui.auteur;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.repositories.PanierRepository;
import ht.mbds.applef.ui.livre.LivreRecyclerViewAdapter;
import ht.mbds.applef.util.BankUtil;

public class AuteurRecyclerViewAdapter extends RecyclerView.Adapter<AuteurRecyclerViewAdapter.ViewHolder> {

    private final List<Author> lAuthors;
    private AuteurRecyclerViewAdapter.MyClickListener myClickListener;
    private Context context ;
    AuthorRepository authorRepository = new AuthorRepository();

    Executor executor = Executors.newSingleThreadExecutor();


    public AuteurRecyclerViewAdapter(List<Author> authors,  AuteurRecyclerViewAdapter.MyClickListener myClickListener) {
        lAuthors = authors;
        this.myClickListener=myClickListener;
    }

    @NonNull
    @Override
    public AuteurRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.auteur_fragment_body, parent, false);
        context= parent.getContext();
        return new AuteurRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AuteurRecyclerViewAdapter.ViewHolder holder, int position) {

        Author author = lAuthors.get(position);

        holder.textid_authorId_author.setText(String.valueOf(author.getAuthorId()));
        holder.textid_name_author.setText(String.valueOf(author.getName()));
        holder.textid_email_author.setText(String.valueOf(author.getEmail()));
//        holder.textid_website_author.setText(String.valueOf(author.getWebsite()));

        holder.buttonid_modifier_author.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLivreForm = new Intent(context , FormActivity.class);
                intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_AUTEUR);
                intentLivreForm.putExtra(BankUtil.KEY_ID, author.getAuthorId());
                context.startActivity(intentLivreForm);
            }
        });

        if(!MainActivity.user.getRoles()[0].equalsIgnoreCase("ADMIN") && !MainActivity.user.getRoles()[0].equalsIgnoreCase("ROLE_ADMIN")){
            holder.buttonid_modifier_author.setVisibility(View.GONE);
        }

        holder.buttonid_liste_livre_author.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return lAuthors.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        View view;
        public TextView textid_authorId_author ;
        public  TextView textid_name_author ;
        public  TextView textid_email_author ;
        public  TextView textid_website_author ;

        public Button buttonid_modifier_author;
        public Button buttonid_liste_livre_author;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            textid_authorId_author=view.findViewById(R.id.id_authorId_author);
            textid_name_author=view.findViewById(R.id.id_name_author);
            textid_email_author=view.findViewById(R.id.id_email_author);
            textid_website_author=view.findViewById(R.id.id_genre_livre);
            buttonid_modifier_author=view.findViewById(R.id.id_modifier_author);
            buttonid_liste_livre_author=view.findViewById(R.id.id_liste_livre_author);
        }

        //   @BindView(R.id.idTitre)

        @Override
        public void onClick(View view) {

        }
    }

    public interface MyClickListener {
        void onItemClick(Long idNeighbour);
    }
}
