package ht.mbds.applef.ui.panier;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.dto.Panier;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.repositories.PanierRepository;

public class PanierViewModel extends ViewModel {
    PanierRepository repository = new PanierRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<List<Panier>> listPanier = new MutableLiveData<>();
    public LiveData<List<Panier>> getListPanier() {
        return listPanier;
    }

    public void getPaniers() {
         executor.execute(() -> {
            try {
                // Récupérer la liste des paniers via le repository
                List<Panier> paniers = repository.getPaniers();
                // Mettre à jour le live data
                listPanier.postValue(paniers);
            } catch (IOException e) {

                e.printStackTrace();
            }
        });
    }

}