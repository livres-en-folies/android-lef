package ht.mbds.applef.ui.auteur;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Author;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.repositories.LivreRepository;

public class AuteurViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    AuthorRepository authorRepository = new AuthorRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<List<Author>> listAuthor = new MutableLiveData<>();

    public LiveData<List<Author>> getListAuthor() {

        return listAuthor;
    }

    public void getAuthors() {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                // Récupérer la liste des articles via le repository
                List<Author> authors = authorRepository.getAuteurs();
                // Mettre à jour le live data
                listAuthor.postValue(authors);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}