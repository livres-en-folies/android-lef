package ht.mbds.applef.ui.livre;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.LivreRepository;

public class LivreViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    LivreRepository repository = new LivreRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<List<Livre>> listLivre = new MutableLiveData<>();

    public LiveData<List<Livre>> getListLivre() {
        return listLivre;
    }


    public void getLivres() {
        executor.execute(() -> {
            try {
                List<Livre> livres = repository.getLivres();
                listLivre.postValue(livres);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void getLivresByAuteur(int idAuteur) {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                List<Livre> livres = repository.getLivres();
                listLivre.postValue(livres);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void getLivresByCategorie(int idCategorie) {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                List<Livre> livres = repository.getLivres();
                listLivre.postValue(livres);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }




}