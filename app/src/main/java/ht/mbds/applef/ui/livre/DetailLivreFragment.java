package ht.mbds.applef.ui.livre;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.util.BankUtil;

public class DetailLivreFragment extends Fragment {

    private DetailLivreViewModel mViewModel;
    private int idLivre=0;
    private Context context =null;
    private TextView textTitreLivre;

    private TextView textid_nom_auteur;
    private TextView textid_publisher;
    private TextView textid_prix;
    private TextView textid_libelle_summary;
    private Button buttonModifier ;
    private FloatingActionButton fab;



    public static DetailLivreFragment newInstance() {
        return new DetailLivreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.detail_livre_fragment, container, false);
        idLivre= getArguments().getInt(BankUtil.KEY_ID);
        textTitreLivre = view.findViewById(R.id.nom_livre);
        textid_nom_auteur= view.findViewById(R.id.id_nom_auteur);
        textid_publisher= view.findViewById(R.id.id_publisher);
        textid_prix= view.findViewById(R.id.id_prix);
        textid_libelle_summary= view.findViewById(R.id.id_libelle_summary);
        fab = view.findViewById(R.id.edit_livre);

    //    buttonModifier = view.findViewById(R.id.id_modifier_livre);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context= getContext();
        mViewModel = new ViewModelProvider(this).get(DetailLivreViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toast.makeText(getContext(), "GOD the id = "+idLivre, Toast.LENGTH_LONG).show();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLivreForm = new Intent(context , FormActivity.class);
                intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_LIVRE);
                intentLivreForm.putExtra(BankUtil.KEY_ID, idLivre);
                context.startActivity(intentLivreForm);
            }
        });

        if(!MainActivity.user.getRoles()[0].equalsIgnoreCase("ADMIN") && !MainActivity.user.getRoles()[0].equalsIgnoreCase("ROLE_ADMIN")){
            fab.setVisibility(View.GONE);
        }

        //Recuperation Livre
        mViewModel.getOneLivre().observe(getViewLifecycleOwner(), new Observer<Livre>() {
            @Override
            public void onChanged(Livre livre) {


                textTitreLivre.setText(livre.getTitle());
                textid_nom_auteur.setText(livre.getAuthor().getName());
                textid_publisher.setText(livre.getPublisher());
                textid_prix.setText(String.valueOf(livre.getPrice()));
                textid_libelle_summary.setText(livre.getSummary());

            }
        });

        if(idLivre!=0){
            mViewModel.getLivre(idLivre);
        }
    }
}