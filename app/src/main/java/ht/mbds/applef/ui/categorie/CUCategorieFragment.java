package ht.mbds.applef.ui.categorie;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.R;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.util.BankUtil;

public class CUCategorieFragment extends Fragment {

    private CUCategorieViewModel mViewModel;

    private int idCategorie=0;

    private CategorieRepository categorieRepository =new CategorieRepository();
    private Context context =null;
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();


    private EditText eid_id_cid;
    private EditText eid_cname;
    private Button buttonValider;


    public static CUCategorieFragment newInstance() {
        return new CUCategorieFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.c_u_categorie_fragment, container, false);

        idCategorie= getArguments().getInt(BankUtil.KEY_ID);
        eid_id_cid = view.findViewById(R.id.id_cid);
        eid_cname  = view.findViewById(R.id.id_cname);
        buttonValider = view.findViewById(R.id.id_create_categorie);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
     //   mViewModel = new ViewModelProvider(this).get(CUCategorieViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CUCategorieViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Recuperation Livre
        mViewModel.getOneCategorie().observe(getViewLifecycleOwner(), new Observer<Categorie>() {
            @Override
            public void onChanged(Categorie categorie) {
                eid_id_cid.setText(String.valueOf(categorie.getGenreId()));
                eid_cname.setText(categorie.getName());
            }
        });

        if(idCategorie!=0){
            mViewModel.getCategorie(idCategorie);
        }

        // Initialisation avec id livre --> Si creation egal zero
        eid_id_cid.setText("0");

        buttonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eid_id_cid==null || eid_id_cid.getText().toString().equalsIgnoreCase("")){
                    eid_id_cid.setError("Champs obligatoire");
                    return;
                }

                if(eid_cname==null || eid_cname.getText().toString().equalsIgnoreCase("")){
                    eid_cname.setError("Champ obligatoire");
                    return;
                }

                Categorie categorie= new Categorie();
                categorie.setGenreId(Integer.valueOf(eid_id_cid.getText().toString()));
                categorie.setName(eid_cname.getText().toString());

                executor.execute(() -> {
                    try {
                            if(idCategorie==0){
                                categorieRepository.ajouterCategorie(categorie);
                            }else{
                                categorieRepository.modifierCategorie(categorie);
                            }

                            notifieMe(200, "Operation reussie");
                    }catch (Exception e){
                        notifieMe(400, "Operation echouee");
                        System.out.println("GOD my life "+e.getMessage());
                    }
                });








            }
        });

    }



    public void notifieMe(int code, String message){
        handler.post(new Runnable() {
            @Override
            public void run() {

                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            getActivity().finish();

            }
        });

    }


}