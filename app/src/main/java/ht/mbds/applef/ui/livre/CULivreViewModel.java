package ht.mbds.applef.ui.livre;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.repositories.LivreRepository;

public class CULivreViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    AuthorRepository authorRepository = new AuthorRepository();
    CategorieRepository categorieRepository = new CategorieRepository();
    LivreRepository livreRepository = new LivreRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<List<Author>> listAuthor = new MutableLiveData<>();
    private MutableLiveData<List<Categorie>> listCategorie= new MutableLiveData<>();
    private MutableLiveData<Livre> livre = new MutableLiveData<>();

    public LiveData<List<Author>> getListAuthor() {
        return listAuthor;
    }
    public LiveData<List<Categorie>> getListCategorie() {
        return listCategorie;
    }

    public LiveData<Livre> getOneLivre(){
        return livre;
    }

    public void getAuthors() {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                // Récupérer la liste des articles via le repository
                List<Author> authors = authorRepository.getAuteurs();
                // Mettre à jour le live data
                listAuthor.postValue(authors);
            } catch (IOException e) {
                // TODO: Gérer les erreurs liées au chargement de la liste des livre
                // Pour cela, il faut utiliser un autre live data (errorLiveData) qui notifiera le
                // fragment quand il y a une erreur
                // TODO Observer le live data dans le fragment pour afficher un message à l'utilisateur
                e.printStackTrace();
            }
        });
    }


    public void getCatgories() {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                // Récupérer la liste des articles via le repository
                List<Categorie> categories = categorieRepository.getCategories();
                // Mettre à jour le live data
                listCategorie.postValue(categories);
            } catch (IOException e) {
                // TODO: Gérer les erreurs liées au chargement de la liste des livre
                // Pour cela, il faut utiliser un autre live data (errorLiveData) qui notifiera le
                // fragment quand il y a une erreur
                // TODO Observer le live data dans le fragment pour afficher un message à l'utilisateur
                e.printStackTrace();
            }
        });
    }

    public void getLivre(int id) {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                // Récupérer la liste des articles via le repository
                Livre livr = livreRepository.getLivre(id);
                // Mettre à jour le live data
                livre.postValue(livr);
            } catch (IOException e) {
                // TODO: Gérer les erreurs liées au chargement de la liste des livre
                // Pour cela, il faut utiliser un autre live data (errorLiveData) qui notifiera le
                // fragment quand il y a une erreur
                // TODO Observer le live data dans le fragment pour afficher un message à l'utilisateur
                e.printStackTrace();
            }
        });
    }

}