package ht.mbds.applef.ui.panier;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.EnvoyerPanier;
import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.dto.Panier;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.repositories.PanierRepository;
import ht.mbds.applef.ui.livre.LivreRecyclerViewAdapter;
import ht.mbds.applef.util.BankUtil;

public class PanierRecyclerViewAdapter  extends RecyclerView.Adapter<PanierRecyclerViewAdapter.ViewHolder>  {

    private final List<Panier> panierList;
    private PanierRecyclerViewAdapter.MyClickListener myClickListener;
    private Context context ;
    PanierRepository panierRepository = new PanierRepository();

    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();

    public PanierRecyclerViewAdapter(List<Panier> items,  PanierRecyclerViewAdapter.MyClickListener myClickListener) {
        panierList = items;
        this.myClickListener=myClickListener;
    }


    @NonNull
    @Override
    public PanierRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.panier_fragment_body, parent, false);
        context= parent.getContext();
        return new PanierRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PanierRecyclerViewAdapter.ViewHolder holder, int position) {
        Panier lePanier = panierList.get(position);
        holder.textid_basket_id.setText(String.valueOf(lePanier.getBasketId()));
        holder.textid_bookid_livre.setText(String.valueOf(lePanier.getBook().getBookId()));
        holder.textid_titre_livre_panier.setText(lePanier.getBook().getTitle());
        //holder.textid_auteur_livre_panier.setText(lePanier.getBook().getAuthor().getName());
        //holder.textid_genre_livre_panier.setText(lePanier.getBook().getGenre().getName());
        holder.textid_edition_livre_panier.setText(lePanier.getBook().getEdition()+"/"+lePanier.getBook().getYear());
        holder.textid_price_livre_panier.setText(String.valueOf(lePanier.getBook().getPrice()));


        holder.iB_id_delete_panier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("GOD my life click "+ holder.textid_titre_livre_panier.getText() );

                executor.execute(() -> {
                    try {
                        panierRepository.deleteOnePanier(Integer.valueOf(holder.textid_basket_id.getText().toString()));
                        myFunction();
                    }catch (Exception e){
                        myFunction();
                        System.out.println("GOD my life "+e.getMessage());
                    }
                });


            }
        });

     //   Toast.makeText(context, "GOD "+panierList.size(), Toast.LENGTH_LONG).show();
    }

    @Override
    public int getItemCount() {
        return panierList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        View view;
        public TextView textid_basket_id;
        public TextView textid_bookid_livre;
        public TextView textid_titre_livre_panier;
        public TextView textid_auteur_livre_panier;
        public TextView textid_genre_livre_panier;
        public TextView textid_edition_livre_panier;
        public TextView textid_price_livre_panier;



        public ImageButton iB_id_delete_panier;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            textid_basket_id=view.findViewById(R.id.id_basketid);
            textid_bookid_livre=view.findViewById(R.id.id_bookid_livre_panier);
            textid_titre_livre_panier=view.findViewById(R.id.id_titre_livre_panier);
        /*    textid_auteur_livre_panier=view.findViewById(R.id.id_auteur_livre_panier);
            textid_genre_livre_panier=view.findViewById(R.id.id_genre_livre_panier);*/
            textid_edition_livre_panier=view.findViewById(R.id.id_edition_livre_panier);
            textid_price_livre_panier=view.findViewById(R.id.id_price_livre_panier);
            iB_id_delete_panier=view.findViewById(R.id.id_delete_panier);
        }

        @Override
        public void onClick(View view) {

        }
    }

    public interface MyClickListener {
        void onItemClick(Long idNeighbour);
    }


    public void myFunction(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                System.out.println("GOD le click adap");
                myClickListener.onItemClick(0L);
            }
        });
    }

}
