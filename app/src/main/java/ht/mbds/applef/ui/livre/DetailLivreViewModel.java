package ht.mbds.applef.ui.livre;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.LivreRepository;

public class DetailLivreViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    LivreRepository livreRepository = new LivreRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<Livre> livre = new MutableLiveData<>();


    public LiveData<Livre> getOneLivre(){
        return livre;
    }

    public void getLivre(int id) {
        executor.execute(() -> {
            try {
                Livre livr = livreRepository.getLivre(id);
                livre.postValue(livr);

            } catch (IOException e) {

                e.printStackTrace();
            }
        });
    }
}