package ht.mbds.applef.ui.categorie;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;
import ht.mbds.applef.ui.auteur.AuteurRecyclerViewAdapter;
import ht.mbds.applef.ui.livre.LivreRecyclerViewAdapter;
import ht.mbds.applef.util.BankUtil;

public class CategorieRecyclerViewAdapter extends RecyclerView.Adapter<CategorieRecyclerViewAdapter.ViewHolder> {

    private final List<Categorie> lCategorie;
    private CategorieRecyclerViewAdapter.MyClickListener myClickListener;
    private Context context ;
    CategorieRepository categorieRepository = new CategorieRepository();
    Executor executor = Executors.newSingleThreadExecutor();


    public CategorieRecyclerViewAdapter(List<Categorie> categories,  CategorieRecyclerViewAdapter.MyClickListener myClickListener) {
        lCategorie = categories;
        this.myClickListener=myClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categorie_fragment_body, parent, false);
        context= parent.getContext();
        return new CategorieRecyclerViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Categorie categorie = lCategorie.get(position);

         holder.textid_genre_id_categorie.setText(String.valueOf(categorie.getGenreId()));
         holder.textid_name_categorie.setText(categorie.getName());
         holder.buttonid_modifier_categorie.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intentLivreForm = new Intent(context , FormActivity.class);
                 intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_CATEGORIE);
                 intentLivreForm.putExtra(BankUtil.KEY_ID, Integer.valueOf(holder.textid_genre_id_categorie.getText().toString()));
                 context.startActivity(intentLivreForm);
             }
         });

        if(!MainActivity.user.getRoles()[0].equalsIgnoreCase("ADMIN") && !MainActivity.user.getRoles()[0].equalsIgnoreCase("ROLE_ADMIN")){
            holder.buttonid_modifier_categorie.setVisibility(View.GONE);
        }

         holder.buttonid_liste_livre_categorie.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

             }
         });
    }

    @Override
    public int getItemCount() {
        return lCategorie.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        View view;
        public TextView textid_genre_id_categorie ;
        public  TextView textid_name_categorie ;
        public Button buttonid_modifier_categorie;
        public Button buttonid_liste_livre_categorie;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            textid_genre_id_categorie=view.findViewById(R.id.id_genre_id_categorie);
            textid_name_categorie=view.findViewById(R.id.id_name_categorie);
            buttonid_modifier_categorie=view.findViewById(R.id.id_modifier_categorie);
            buttonid_liste_livre_categorie=view.findViewById(R.id.id_liste_livre_categorie);

        }

        //   @BindView(R.id.idTitre)

        @Override
        public void onClick(View view) {

        }
    }

    public interface MyClickListener {
        void onItemClick(Long idNeighbour);
    }
}
