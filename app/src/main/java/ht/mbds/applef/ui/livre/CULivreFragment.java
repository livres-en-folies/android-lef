package ht.mbds.applef.ui.livre;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.util.BankUtil;

public class CULivreFragment extends Fragment {

    private CULivreViewModel mViewModel;
    private int idLivre=0;

    private LivreRepository livreRepository =new LivreRepository();
    private Context context =null;
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();

    private EditText eid_idL;
    private EditText eid_isbn;
    private EditText eid_title;
    private EditText eid_edition;
    private EditText eid_publisher;
    private EditText eid_year;
    private EditText eid_price;
    private EditText eid_summary;
    private Button buttonValider ;
    private Spinner spinnerAuteur;
    private Spinner spinnerCategorie;


    public static CULivreFragment newInstance() {
        return new CULivreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view=  inflater.inflate(R.layout.c_u_livre_fragment, container, false);

         idLivre= getArguments().getInt(BankUtil.KEY_ID);
         eid_idL = view.findViewById(R.id.id_idL);
         eid_isbn  = view.findViewById(R.id.id_isbn);
         eid_title  = view.findViewById(R.id.id_title);
         eid_edition  = view.findViewById(R.id.id_edition);
         eid_publisher  = view.findViewById(R.id.id_publisher);
         eid_year  = view.findViewById(R.id.id_year);
         eid_price  = view.findViewById(R.id.id_price);
         eid_summary  = view.findViewById(R.id.id_summary);
         spinnerAuteur = view.findViewById(R.id.id_auteur);
         spinnerCategorie = view.findViewById(R.id.id_categorie);
         buttonValider = view.findViewById(R.id.createLivre);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
     //   mViewModel = new ViewModelProvider(this).get(CULivreViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context= getContext();
        mViewModel = new ViewModelProvider(this).get(CULivreViewModel.class);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // Recuperation Autheur
        mViewModel.getListAuthor().observe(getViewLifecycleOwner(), new Observer<List<Author>>() {
            @Override
            public void onChanged(List<Author> authors) {

                // Creation Adapter Author
                ArrayAdapter<Author> dataAdapter = new ArrayAdapter<Author>(context, android.R.layout.simple_spinner_item, authors);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerAuteur.setAdapter(dataAdapter);
            }
        });
        mViewModel.getAuthors();

        // Recuperation Categorie
        mViewModel.getListCategorie().observe(getViewLifecycleOwner(), new Observer<List<Categorie>>() {
            @Override
            public void onChanged(List<Categorie> categories) {
                // Creation Adapteur Categorie
                ArrayAdapter<Categorie> dataAdapter = new ArrayAdapter<Categorie>(context, android.R.layout.simple_spinner_item, categories);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerCategorie.setAdapter(dataAdapter);
            }
        });
        mViewModel.getCatgories();

        //Recuperation Livre
        mViewModel.getOneLivre().observe(getViewLifecycleOwner(), new Observer<Livre>() {
            @Override
            public void onChanged(Livre livre) {
                eid_idL.setText(String.valueOf(livre.getBookId()));
                eid_isbn.setText(livre.getIsbn());
                eid_title.setText(livre.getTitle());
                eid_edition.setText(livre.getEdition());
                eid_publisher.setText(livre.getPublisher());
                eid_year.setText(String.valueOf(livre.getYear()));
                eid_price.setText(String.valueOf(livre.getPrice()));
                eid_summary.setText(livre.getSummary());
                spinnerAuteur.setSelection(getIndex(spinnerAuteur, livre.getAuthor()));
            }
        });

        if(idLivre!=0){
            mViewModel.getLivre(idLivre);
        }

        // Initialisation avec id livre --> Si creation egal zero
        eid_idL.setText("0");

        buttonValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executor.execute(() -> {
                    try {

                          /*  if(eid_idL==null || eid_idL.getText() == null || eid_idL.getText().toString().trim().equals("")){
                                eid_idL.setError("Ce champs est obligatoire");
                                return;
                            }*/

                        if(eid_isbn==null || eid_isbn.getText() == null || eid_isbn.getText().toString().trim().equals("")){
                            eid_isbn.setError("Ce champs est obligatoire");
                            return;
                        }

                        if(eid_title==null || eid_title.getText() == null || eid_title.getText().toString().trim().equals("")){
                            eid_title.setError("Ce champs est obligatoire");
                            return;
                        }

                        if(eid_edition==null || eid_edition.getText() == null || eid_edition.getText().toString().trim().equals("")){
                            eid_edition.setError("Ce champs est obligatoire");
                            return;
                        }

                        if(eid_publisher==null || eid_publisher.getText() == null || eid_publisher.getText().toString().trim().equals("")){
                            eid_publisher.setError("Ce champs est obligatoire");
                            return;
                        }


                        if(eid_year==null || eid_year.getText() == null || eid_year.getText().toString().trim().equals("")){
                            eid_year.setError("Ce champs est obligatoire");
                            return;
                        }

                        if(eid_price==null || eid_price.getText() == null || eid_price.getText().toString().trim().equals("")){
                            eid_price.setError("Ce champs est obligatoire");
                            return;
                        }

                        if(eid_summary==null || eid_summary.getText() == null || eid_summary.getText().toString().trim().equals("")){
                            eid_summary.setError("Ce champs est obligatoire");
                            return;
                        }

                        Livre livre = new Livre();
                        livre.setBookId(Integer.valueOf(eid_idL.getText().toString()));
                        livre.setIsbn(eid_isbn.getText().toString());
                        livre.setTitle(eid_title.getText().toString());
                        livre.setEdition(eid_edition.getText().toString());
                        livre.setPublisher(eid_publisher.getText().toString());
                        livre.setYear(Integer.valueOf(eid_year.getText().toString()));
                        livre.setPrice(Double.valueOf(eid_price.getText().toString()));
                        livre.setSummary(eid_summary.getText().toString());

                        Categorie categorie = (Categorie) spinnerCategorie.getSelectedItem();
                        livre.setGenre(categorie);

                        Author author = (Author) spinnerAuteur.getSelectedItem();
                        livre.setAuthor(author);

                        if(eid_idL.getText().toString().equalsIgnoreCase("0")){
                            livreRepository.ajouterLivre(livre);
                        }else{
                            livreRepository.modifierLivre(livre);
                        }

                    }catch (Exception e){
                        System.out.println("GOD my life "+e.getMessage());
                    }
                });
            }
        });
    }

    private int getIndex(Spinner spinner, Author myAuth){
        for (int i=0;i<spinner.getCount();i++){

            Author author = (Author) spinner.getItemAtPosition(i);
           if (author.getAuthorId() ==  myAuth.getAuthorId()){
                return i;
            }
        }
        return 0;
    }

    public void notifieMe(){
        handler.post(new Runnable() {
            @Override
            public void run() {

            }
        });

    }
}