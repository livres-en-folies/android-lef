package ht.mbds.applef.ui.livre;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import butterknife.BindView;
import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.DataConnexion;
import ht.mbds.applef.dto.EnvoyerPanier;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.dto.User;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.repositories.PanierRepository;
import ht.mbds.applef.repositories.UserRepository;
import ht.mbds.applef.util.BankUtil;

public class LivreRecyclerViewAdapter  extends RecyclerView.Adapter<LivreRecyclerViewAdapter.ViewHolder>  {

    private final List<Livre> llivres;
    private  MyClickListener myClickListener;
    private Context  context ;


    LivreRepository livreRepository = new LivreRepository();
    PanierRepository panierRepository = new PanierRepository();

    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();

    public LivreRecyclerViewAdapter(List<Livre> items,  MyClickListener myClickListener) {
        llivres = items;
        this.myClickListener=myClickListener;
    }


    @NonNull
    @Override
    public LivreRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.livre_fragment_body, parent, false);
        context= parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LivreRecyclerViewAdapter.ViewHolder holder, int position) {
        Livre leLivre = llivres.get(position);
         holder.textbookid.setText(String.valueOf(leLivre.getBookId()));
         holder.textTitre.setText(leLivre.getTitle());
         holder.textAuteur.setText("Ecrit par : "+leLivre.getAuthor().getName());
         holder.textGenre.setText(leLivre.getGenre().getName());
         holder.testEdition.setText(leLivre.getEdition()+" / "+leLivre.getYear());
         holder.buttonDetail.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 System.out.println("GOD my life click "+ holder.textTitre.getText() +" / "+ holder.textbookid.getText());
                Intent intentLivreForm = new Intent(context , FormActivity.class);
                 intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_DETAIL_LIVRE);
                 intentLivreForm.putExtra(BankUtil.KEY_ID, Integer.valueOf(holder.textbookid.getText().toString()));
                 context.startActivity(intentLivreForm);
             }
         });
        holder.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executor.execute(() -> {
                  try{
                      EnvoyerPanier envoyerPanier = new EnvoyerPanier();
                      envoyerPanier.setBookId(Integer.valueOf(holder.textbookid.getText().toString()) );
                      envoyerPanier.setQuantity(1);
                      envoyerPanier.setUserId(MainActivity.user.getId());
                      panierRepository.ajouterPanier(envoyerPanier);
                      myFunctioAjouLivre("Vous avez Aouter le Livre "+holder.textTitre.getText().toString()+" au panier ");
                     }catch (Exception exception){
                         System.out.println("In Exception");
                         System.out.println(exception.getMessage());
                         System.out.println(exception.toString());
                         System.out.println( exception.getStackTrace().toString());
                     }

                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return llivres.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        View view;
        public  TextView textbookid ;
        public  TextView textTitre ;
        public  TextView textAuteur ;
        public  TextView textGenre ;
        public  TextView testEdition;
        public Button buttonDetail;
        public Button buttonAdd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            textbookid=view.findViewById(R.id.id_bookid_livre);
            textTitre=view.findViewById(R.id.id_titre_livre);
            textAuteur=view.findViewById(R.id.id_auteur_livre);
            textGenre=view.findViewById(R.id.id_genre_livre);
            testEdition=view.findViewById(R.id.id_edition_livre);
            buttonDetail=view.findViewById(R.id.id_detail_livre);
            buttonAdd=view.findViewById(R.id.id_add_livre);
        }

     //   @BindView(R.id.idTitre)

        @Override
        public void onClick(View view) {

        }
    }

    public interface MyClickListener {
        void onItemClick(Long idNeighbour);
    }


    public void myFunction(Livre live){
        handler.post(new Runnable() {
            @Override
            public void run() {
                    System.out.println("GOD my life : second "+live.getTitle());
            }
        });
    }

    public void myFunctioAjouLivre(String message){
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });
    }

}
