package ht.mbds.applef.ui.connexion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.DataConnexion;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.dto.User;
import ht.mbds.applef.repositories.UserRepository;
import ht.mbds.applef.util.BankUtil;

public class LoginActivity extends AppCompatActivity {


    private EditText eLogin;
    private EditText ePassword;
    private Button bConnexion;
    private Button bRegister;
    UserRepository userRepository = new UserRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();

    private Context context ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = getApplicationContext();


         eLogin = findViewById(R.id.id_login);
         ePassword = findViewById(R.id.id_password);
         bConnexion = findViewById(R.id.id_connexion);
         bRegister = findViewById(R.id.id_register);


        bConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eLogin==null || eLogin.getText() == null || eLogin.getText().toString().trim().equals("")){
                    eLogin.setError("Ce champs est obligatoire");
                    return;
                }

                if(ePassword==null || ePassword.getText() == null || ePassword.getText().toString().trim().equals("")){
                    ePassword.setError("Ce champs est obligatoire");
                    return;
                }

                        executor.execute(() -> {
                        DataConnexion dataConnexion = new DataConnexion();
                        dataConnexion.setUsername(eLogin.getText().toString().trim());
                        dataConnexion.setPassword(ePassword.getText().toString().trim());
                        try{
                            User  user= userRepository.connexion(dataConnexion);
                            System.out.println(user.toString());
                            myFunction(user);

                        }catch (Exception exception){

                            System.out.println("In Exception");
                            System.out.println(exception.getMessage());
                            System.out.println(exception.toString());
                            System.out.println( exception.getStackTrace().toString());
                        }



                });



            }
        });

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLivreForm = new Intent(context , FormActivity.class);
                intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_REGISTER);
                intentLivreForm.putExtra(BankUtil.KEY_ID, 0);
                startActivity(intentLivreForm);
            }
        });

    }


    public void myFunction(User user){
        handler.post(new Runnable() {
            @Override
            public void run() {

                MainActivity.KEY_ACCESS=user.getAccessToken();
                MainActivity.user=user;
                Intent intentMain = new Intent(context, MainActivity.class);
                intentMain.putExtra(BankUtil.DATA_CONNEXION, user);
                startActivity(intentMain);
            }
        });
    }

    public interface Callback<R> {
        void onComplete(R result);
    }


}