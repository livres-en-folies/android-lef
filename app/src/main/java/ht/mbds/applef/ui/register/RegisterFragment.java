package ht.mbds.applef.ui.register;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.R;
import ht.mbds.applef.datasources.models.UserModel;
import ht.mbds.applef.repositories.UserRepository;

public class RegisterFragment extends Fragment {

    private RegisterViewModel mViewModel;
UserRepository userRepository = new UserRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();
    private EditText eid_nom;
    private EditText eid_email;
    private EditText eid_username;
    private EditText eid_password;
    private Button buttonValider;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.register_fragment, container, false);
        eid_nom=view.findViewById(R.id.id_aname_rg);
        eid_email=view.findViewById(R.id.id_aemail_rg);
        eid_username=view.findViewById(R.id.id_ausername_rg);
        eid_password=view.findViewById(R.id.id_apassword_rg);
        buttonValider=view.findViewById(R.id.id_create_user);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonValider.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
              if(eid_nom==null || eid_nom.getText().toString().trim().equalsIgnoreCase(""))  {
                  eid_nom.setError("Champ obligatoire");
                  return;
              }

             if(eid_email==null || eid_email.getText().toString().trim().equalsIgnoreCase(""))  {
                 eid_email.setError("Champ obligatoire");
                    return;
                }

                if(eid_username==null || eid_username.getText().toString().trim().equalsIgnoreCase(""))  {
                    eid_username.setError("Champ obligatoire");
                    return;
                }

                if(eid_password==null || eid_password.getText().toString().trim().equalsIgnoreCase(""))  {
                    eid_password.setError("Champ obligatoire");
                    return;
                }


                UserModel userModel = new UserModel();
                userModel.setName(eid_nom.getText().toString());
                userModel.setEmail(eid_email.getText().toString());
                userModel.setUsername(eid_username.getText().toString());
                userModel.setPassword(eid_password.getText().toString());

                userModel.setRoles( new String[]{"user"});
                executor.execute(() -> {
                    try {
                        userRepository.register(userModel);
                        notifieMe();
                    }catch (Exception exception){

                    }
                });



            }
        });


    }


    public void notifieMe(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext() , "Operation reussie", Toast.LENGTH_SHORT).show();
               getActivity().finish();
            }
        });

    }
}