package ht.mbds.applef.ui.panier;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.dto.Panier;
import ht.mbds.applef.repositories.InitTransactionRepository;
import ht.mbds.applef.ui.livre.LivreFragment;
import ht.mbds.applef.ui.livre.LivreRecyclerViewAdapter;
import ht.mbds.applef.ui.livre.LivreViewModel;
import ht.mbds.applef.util.BankUtil;

public class PanierFragment extends Fragment  implements PanierRecyclerViewAdapter.MyClickListener {

    private PanierViewModel mViewModel;
    private RecyclerView mRecyclerView;
    private FloatingActionButton fab;
    private Context context =null;
    PanierFragment ph ;

    List<Panier> paniersTmp;
    Executor executor = Executors.newSingleThreadExecutor();
    private final Handler handler = new Handler();

    InitTransactionRepository initTransactionRepository = new InitTransactionRepository();


    public static PanierFragment newInstance() {
        return new PanierFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.panier_fragment, container, false);

         context = view.getContext();
        ph=this;
        fab = view.findViewById(R.id.id_but_checkout_panier);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_panier);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
      //  mViewModel = new ViewModelProvider(this).get(PanierViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(PanierViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PanierFragment fragment = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(paniersTmp==null || paniersTmp.size() == 0){
                    Toast.makeText(context, "Votre panier est vide", Toast.LENGTH_SHORT ).show();
                }else{
                    //Toast.makeText(context, "GOD > Votre panier est pleine "+paniersTmp.size(), Toast.LENGTH_SHORT ).show();
                /*    executor.execute(() -> {
                        try {*/
                            InitTransaction initTransaction = new InitTransaction();
                            double somme = 0;
                            for (Panier pp: paniersTmp) {
                                somme = somme+  (pp.getQuantity()*pp.getBook().getPrice());
                            }
                            Intent intentLivreForm = new Intent(fragment.getContext() , FormActivity.class);
                            intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_PAIEMENT);
                            intentLivreForm.putExtra(BankUtil.KEY_ID, 0);
                            intentLivreForm.putExtra(BankUtil.KEY_MONTANT_PAIEMENT, somme);
                            startActivity(intentLivreForm);
                    /*
                        }catch (Exception e){
                            System.out.println("GOD my life "+e.getMessage());
                        }
                    });*/

                }



            }
        });

        mViewModel.getListPanier().observe(getViewLifecycleOwner(), new Observer<List<Panier>>() {
            @Override
            public void onChanged(List<Panier> paniers) {
                paniersTmp=paniers;
                mRecyclerView.setAdapter(new PanierRecyclerViewAdapter(paniers, ph));
            }
        });

        mViewModel.getPaniers();
    }

    @Override
    public void onItemClick(Long idNeighbour) {
        System.out.println("GOD le click frag");
        mViewModel.getPaniers();
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("GOD __ P");
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("GOD __ R");
        mViewModel.getPaniers();
    }
}