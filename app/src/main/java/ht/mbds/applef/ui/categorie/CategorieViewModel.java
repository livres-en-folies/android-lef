package ht.mbds.applef.ui.categorie;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.repositories.AuthorRepository;
import ht.mbds.applef.repositories.CategorieRepository;

public class CategorieViewModel extends ViewModel {
    CategorieRepository categorieRepository = new CategorieRepository();
    Executor executor = Executors.newSingleThreadExecutor();
    private MutableLiveData<List<Categorie>> listCategorie = new MutableLiveData<>();


    public LiveData<List<Categorie>> getListCategorie() {

        return listCategorie;
    }

    public void getCategories() {
        // Le chargement des articles droit se faire dans un thread secondaire car il y une connexion à internet
        executor.execute(() -> {
            try {
                // Récupérer la liste des articles via le repository
                List<Categorie> categories = categorieRepository.getCategories();
                // Mettre à jour le live data
                listCategorie.postValue(categories);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}