package ht.mbds.applef.ui.categorie;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import ht.mbds.applef.FormActivity;
import ht.mbds.applef.MainActivity;
import ht.mbds.applef.R;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.ui.auteur.AuteurFragment;
import ht.mbds.applef.ui.auteur.AuteurRecyclerViewAdapter;
import ht.mbds.applef.util.BankUtil;

public class CategorieFragment extends Fragment {

    private CategorieViewModel mViewModel;
    private RecyclerView mRecyclerView;
    private FloatingActionButton fab;

    public static CategorieFragment newInstance() {
        return new CategorieFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.categorie_fragment, container, false);
        Context context = view.getContext();
        fab = view.findViewById(R.id.id_but_create_categorie);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_categorie);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
      //  mViewModel = new ViewModelProvider(this).get(CategorieViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CategorieViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CategorieFragment fragment = this;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intentLivreForm = new Intent(fragment.getContext() , FormActivity.class);
                intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_LIVRE);
                intentLivreForm.putExtra(BankUtil.KEY_ID, 0);
                startActivity(intentLivreForm);*/

                Intent intentLivreForm = new Intent(fragment.getContext() , FormActivity.class);
                intentLivreForm.putExtra(BankUtil.KEY_SCREEN, BankUtil.KEY_CATEGORIE);
                intentLivreForm.putExtra(BankUtil.KEY_ID, 0);
                startActivity(intentLivreForm);
            }
        });

        if(!MainActivity.user.getRoles()[0].equalsIgnoreCase("ADMIN") && !MainActivity.user.getRoles()[0].equalsIgnoreCase("ROLE_ADMIN")){
            fab.setVisibility(View.GONE);
        }

        mViewModel.getListCategorie().observe(getViewLifecycleOwner(), new Observer<List<Categorie>>() {
            @Override
            public void onChanged(List<Categorie> categorie) {
                mRecyclerView.setAdapter(new CategorieRecyclerViewAdapter(categorie, null));
            }
        });

        mViewModel.getCategories();

    }
}