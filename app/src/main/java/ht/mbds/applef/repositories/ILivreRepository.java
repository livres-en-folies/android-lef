package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.dto.Livre;
import retrofit2.Call;
import retrofit2.http.POST;

public interface ILivreRepository {

    List<Livre> getLivresByAuthor(int idAuthor) throws IOException;
    List<Livre> getLivresByCategorie(int idCategorie) throws IOException;
    List<Livre> getLivres() throws IOException;
    Livre ajouterLivre(Livre livre) throws IOException;
    Livre modifierLivre(Livre livre) throws IOException;
    Livre getLivre(int id) throws IOException ;

}
