package ht.mbds.applef.repositories;

import java.io.IOException;

import ht.mbds.applef.datasources.UserDataSource;
import ht.mbds.applef.datasources.models.UserModel;
import ht.mbds.applef.dto.DataConnexion;
import ht.mbds.applef.dto.ResponseRegister;
import ht.mbds.applef.dto.User;

public class UserRepository implements IUserRepository {

    UserDataSource userDataSource = new UserDataSource();

    @Override
    public User connexion( DataConnexion dataConnexion) throws IOException {

        UserModel userModel = userDataSource.connexion(dataConnexion);
        User user = new User(
        userModel.getId(),
        userModel.getUsername(),
        userModel.getEmail(),
        userModel.getName(),
        userModel.getPassword(),
        userModel.getAccessToken(),
        userModel.getRoles());
        return user;
    }

    @Override
    public ResponseRegister register(UserModel dt) throws IOException {
        return userDataSource.register(dt);
    }
}
