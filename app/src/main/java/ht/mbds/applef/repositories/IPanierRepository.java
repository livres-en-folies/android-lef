package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.PanierModel;
import ht.mbds.applef.dto.EnvoyerPanier;
import ht.mbds.applef.dto.Panier;

public interface IPanierRepository {

    List<Panier> getPaniers() throws IOException ;

    Panier getOnePanier(int id) throws IOException ;

    List<Panier> getPanierByUser(int id) throws IOException ;

    Panier ajouterPanier( EnvoyerPanier envoyerPanier) throws IOException ;

    Panier deleteOnePanier( int id) throws IOException ;

    Panier modifierOnePanier( int id,  EnvoyerPanier envoyerPanier) throws IOException ;
}
