package ht.mbds.applef.repositories;

import java.io.IOException;

import ht.mbds.applef.datasources.EndTrasactionDataSource;
import ht.mbds.applef.dto.EndTransaction;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;

public class EndTrasactionRepository implements IEndTrasactionRepository {
    EndTrasactionDataSource endTrasactionDataSource = new EndTrasactionDataSource();
    @Override
    public ResponseDTO<ReturnInit> endTransaction(EndTransaction endT) throws IOException {
        return endTrasactionDataSource.endTransaction(endT);
    }
}
