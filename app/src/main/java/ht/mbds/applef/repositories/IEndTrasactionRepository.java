package ht.mbds.applef.repositories;

import java.io.IOException;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.dto.EndTransaction;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;

public interface IEndTrasactionRepository {

    public ResponseDTO<ReturnInit> endTransaction(EndTransaction endT) throws IOException ;
}
