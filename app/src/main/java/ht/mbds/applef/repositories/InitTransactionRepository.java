package ht.mbds.applef.repositories;

import java.io.IOException;

import ht.mbds.applef.datasources.InitTransactionDataSource;
import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;

public class InitTransactionRepository implements IInitTransactionRepository{


    InitTransactionDataSource initTransactionDataSource = new InitTransactionDataSource();
    @Override
    public ResponseDTO<ReturnInit> initTransaction(InitTransaction initT) throws IOException {
        return initTransactionDataSource.initTransaction(initT);
    }
}
