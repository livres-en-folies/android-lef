package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.CategorieModel;
import ht.mbds.applef.dto.Categorie;

public interface ICategorieRepository {
    public List<Categorie> getCategories() throws IOException ;

    public  Categorie getCategorie(int id) throws IOException ;

    public Categorie ajouterCategorie(Categorie categorie) throws IOException ;

    public Categorie modifierCategorie( Categorie categorie) throws IOException ;

}
