package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.dto.InitTransaction;
import ht.mbds.applef.dto.Panier;
import ht.mbds.applef.dto.ResponseDTO;
import ht.mbds.applef.dto.ReturnInit;

public interface IInitTransactionRepository {

    ResponseDTO<ReturnInit> initTransaction(InitTransaction initT) throws IOException;
}
