package ht.mbds.applef.repositories;

import java.io.IOException;

import ht.mbds.applef.datasources.models.UserModel;
import ht.mbds.applef.dto.DataConnexion;
import ht.mbds.applef.dto.ResponseRegister;
import ht.mbds.applef.dto.User;

public interface IUserRepository {
    public User connexion(DataConnexion dataConnexion) throws IOException;
    ResponseRegister register(UserModel dt) throws IOException;
}
