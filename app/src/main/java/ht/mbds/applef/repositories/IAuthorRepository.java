package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.List;

import ht.mbds.applef.MainActivity;
import ht.mbds.applef.datasources.models.AuteurModel;
import ht.mbds.applef.dto.Author;

public interface IAuthorRepository {

     public List<Author> getAuteurs() throws IOException;


    public Author getAuteur(int id)  throws IOException;


    public Author ajouterAuteur( Author author) throws IOException;


    public Author modifierAuteur(Author author) throws IOException ;
}
