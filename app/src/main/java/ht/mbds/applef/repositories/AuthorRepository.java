package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ht.mbds.applef.datasources.AuteurDataSource;
import ht.mbds.applef.datasources.models.AuteurModel;
import ht.mbds.applef.dto.Author;

public class AuthorRepository implements IAuthorRepository {

    AuteurDataSource auteurDataSource = new AuteurDataSource();

    @Override
    public List<Author> getAuteurs() throws IOException {
        List<AuteurModel> auteurModelList = auteurDataSource.getAuteurs();
        List<Author> authorList = new ArrayList<>();
        for (AuteurModel authorTemp : auteurModelList) {
            Author aut = new Author();
            aut.setAuthorId(authorTemp.getAuthorId());
            aut.setEmail(authorTemp.getEmail());
            aut.setName(authorTemp.getName());
            aut.setPhone(authorTemp.getPhone());
            aut.setWebsite(authorTemp.getWebsite());
            authorList.add(aut);
        }
        return authorList;
    }

    @Override
    public Author getAuteur(int id) throws IOException {
       AuteurModel  authorTemp = auteurDataSource.getAuteur(id);
        Author aut = new Author();
        aut.setAuthorId(authorTemp.getAuthorId());
        aut.setEmail(authorTemp.getEmail());
        aut.setName(authorTemp.getName());
        aut.setPhone(authorTemp.getPhone());
        aut.setWebsite(authorTemp.getWebsite());
        return aut;
    }

    @Override
    public Author ajouterAuteur(Author author) throws IOException {
        AuteurModel auteurModel = new AuteurModel();
        auteurModel.setAuthorId(author.getAuthorId());
        auteurModel.setName(author.getName());
        auteurModel.setEmail(author.getEmail());
        auteurModel.setPhone(author.getPhone());
        auteurModel.setWebsite(author.getWebsite());
        auteurDataSource.ajouterAuteur(auteurModel);
        return null;
    }

    @Override
    public Author modifierAuteur(Author author) throws IOException {
        AuteurModel auteurModel = new AuteurModel();
        auteurModel.setAuthorId(author.getAuthorId());
        auteurModel.setName(author.getName());
        auteurModel.setEmail(author.getEmail());
        auteurModel.setPhone(author.getPhone());
        auteurModel.setWebsite(author.getWebsite());
        auteurDataSource.modifierAuteur(auteurModel);
        return null;
    }
}
