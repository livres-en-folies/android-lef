package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ht.mbds.applef.datasources.CategorieDataSource;
import ht.mbds.applef.datasources.models.CategorieModel;
import ht.mbds.applef.dto.Categorie;

public class CategorieRepository implements ICategorieRepository {

    CategorieDataSource categorieDataSource = new CategorieDataSource();

    @Override
    public List<Categorie> getCategories() throws IOException {

       List<CategorieModel> categorieModelList = categorieDataSource.getCategories();

       List<Categorie> categorieList = new ArrayList<>();

        for (CategorieModel ctMTempon: categorieModelList) {
            Categorie categorie = new Categorie();
            categorie.setGenreId(ctMTempon.getGenreId());
            categorie.setName(ctMTempon.getName());
            categorieList.add(categorie);
        }

        return categorieList;
    }

    @Override
    public Categorie getCategorie(int id) throws IOException {

        CategorieModel categorieModel = categorieDataSource.getCategorie(id);
        Categorie categorie = new Categorie();
        categorie.setGenreId(categorieModel.getGenreId());
        categorie.setName(categorieModel.getName());

        return categorie;
    }

    @Override
    public Categorie ajouterCategorie(Categorie categorie) throws IOException {

        CategorieModel categorieModel = new CategorieModel();
        categorieModel.setGenreId(categorie.getGenreId());
        categorieModel.setName(categorie.getName());
        categorieDataSource.ajouterCategorie(categorieModel);
        return null;
    }

    @Override
    public Categorie modifierCategorie(Categorie categorie) throws IOException {
        CategorieModel categorieModel = new CategorieModel();
        categorieModel.setGenreId(categorie.getGenreId());
        categorieModel.setName(categorie.getName());
        categorieDataSource.modifierCategorie(categorie.getGenreId(), categorieModel);
        return null;
    }
}
