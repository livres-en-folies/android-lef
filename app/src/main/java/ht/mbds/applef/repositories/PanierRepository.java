package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ht.mbds.applef.datasources.PanierDataSource;
import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.datasources.models.PanierModel;
import ht.mbds.applef.dto.EnvoyerPanier;
import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.dto.Panier;

public class PanierRepository implements IPanierRepository {

    PanierDataSource panierDataSource = new PanierDataSource();


    @Override
    public List<Panier> getPaniers() throws IOException {
      List<PanierModel> panierModelList =  panierDataSource.getPaniers();
      List<Panier> panierList = new ArrayList<>();

        if(panierModelList==null){
            return new ArrayList<Panier>();
        }

        for ( PanierModel modelPanierTemp : panierModelList) {
            Panier panier = new Panier();
            panier.setBasketId(modelPanierTemp.getBasketId());
            panier.setQuantity(modelPanierTemp.getQuantity());
            panier.setUserId(modelPanierTemp.getUserId());

            LivreModel livreModel = modelPanierTemp.getBook();
            Livre livre = new Livre();

            livre.setBookId(livreModel.getBookId());
            livre.setIsbn(livreModel.getIsbn());
            livre.setTitle(livreModel.getTitle());
            livre.setSummary(livreModel.getSummary());
            livre.setEdition(livreModel.getEdition());
            livre.setPublisher(livreModel.getPublisher());
            livre.setYear(livreModel.getYear());
            livre.setPrice(livreModel.getPrice());
            livre.setImage(livreModel.getImage());

            panier.setBook(livre);

            panierList.add(panier);
        }

      return panierList;
    }

    @Override
    public Panier getOnePanier(int id) throws IOException {
        PanierModel modelPanierTemp = panierDataSource.getOnePanier(id);

        if(modelPanierTemp == null){
            return null;
        }

        Panier panier = new Panier();
        panier.setBasketId(modelPanierTemp.getBasketId());
        panier.setQuantity(modelPanierTemp.getQuantity());
        panier.setUserId(modelPanierTemp.getUserId());

        LivreModel livreModel = modelPanierTemp.getBook();
        Livre livre = new Livre();

        livre.setBookId(livreModel.getBookId());
        livre.setIsbn(livreModel.getIsbn());
        livre.setTitle(livreModel.getTitle());
        livre.setSummary(livreModel.getSummary());
        livre.setEdition(livreModel.getEdition());
        livre.setPublisher(livreModel.getPublisher());
        livre.setYear(livreModel.getYear());
        livre.setPrice(livreModel.getPrice());
        livre.setImage(livreModel.getImage());
        panier.setBook(livre);

        return panier;
    }

    @Override
    public List<Panier> getPanierByUser(int id) throws IOException {
        List<PanierModel> panierModelList =  panierDataSource.getPanierByUser(id);
        List<Panier> panierList = new ArrayList<>();

        if(panierModelList==null){
            return new ArrayList<Panier>();
        }

        for ( PanierModel modelPanierTemp : panierModelList) {
            Panier panier = new Panier();
            panier.setBasketId(modelPanierTemp.getBasketId());
            panier.setQuantity(modelPanierTemp.getQuantity());
            panier.setUserId(modelPanierTemp.getUserId());

            LivreModel livreModel = modelPanierTemp.getBook();
            Livre livre = new Livre();

            livre.setBookId(livreModel.getBookId());
            livre.setIsbn(livreModel.getIsbn());
            livre.setTitle(livreModel.getTitle());
            livre.setSummary(livreModel.getSummary());
            livre.setEdition(livreModel.getEdition());
            livre.setPublisher(livreModel.getPublisher());
            livre.setYear(livreModel.getYear());
            livre.setPrice(livreModel.getPrice());
            livre.setImage(livreModel.getImage());

            panier.setBook(livre);

            panierList.add(panier);
        }

        return panierList;
    }

    @Override
    public Panier ajouterPanier(EnvoyerPanier envoyerPanier) throws IOException {
        panierDataSource.ajouterPanier(envoyerPanier);
        return null;
    }

    @Override
    public Panier deleteOnePanier(int id) throws IOException {
        panierDataSource.deleteOnePanier(id);
        return null;
    }

    @Override
    public Panier modifierOnePanier(int id, EnvoyerPanier envoyerPanier) throws IOException {
        panierDataSource.modifierOnePanier(id,envoyerPanier);
        return null;
    }
}
