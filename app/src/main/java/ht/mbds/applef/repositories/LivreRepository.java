package ht.mbds.applef.repositories;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ht.mbds.applef.datasources.LivreDataSource;
import ht.mbds.applef.datasources.models.AuteurModel;
import ht.mbds.applef.datasources.models.CategorieModel;
import ht.mbds.applef.datasources.models.LivreModel;
import ht.mbds.applef.dto.Author;
import ht.mbds.applef.dto.Categorie;
import ht.mbds.applef.dto.Livre;

public class LivreRepository implements ILivreRepository {

  LivreDataSource dataSource = new LivreDataSource();


    @Override
    public List<Livre> getLivresByAuthor(int idAuthor) throws IOException {
        return null;
    }

    @Override
    public List<Livre> getLivresByCategorie(int idCategorie) throws IOException {
        return null;
    }

    @Override
    public List<Livre> getLivres() throws IOException {
       List<LivreModel>  livreModels = dataSource.getLivres();
       List<Livre> livres  = new ArrayList<>();

        for (LivreModel model: livreModels) {
            AuteurModel auteurModel = model.getAuthor();
            Author author= new Author(auteurModel.getAuthorId(), auteurModel.getName(), auteurModel.getEmail(), auteurModel.getPhone(), auteurModel.getWebsite());
            CategorieModel categorieModel = model.getGenre();
            Categorie categorie = new Categorie(categorieModel.getGenreId(), categorieModel.getName());

            livres.add(new Livre(
                    model.getBookId(),
                    model.getIsbn(),
                    model.getTitle(),
                    model.getSummary(),
                    model.getEdition(),
                    model.getPublisher(),
                    model.getYear(),
                    model.getPrice(),
                    model.getImage(),
                    author,
                    categorie
            ));
        }
        return livres;
    }

    @Override
    public Livre ajouterLivre(Livre livre) throws IOException {
        LivreModel livreModel = new LivreModel();
        livreModel.setBookId(livre.getBookId());
        livreModel.setIsbn(livre.getIsbn());
        livreModel.setTitle(livre.getTitle());
        livreModel.setEdition(livre.getEdition());
        livreModel.setPublisher(livre.getPublisher());
        livreModel.setYear(livre.getYear());
        livreModel.setPrice(livre.getPrice());
        livreModel.setSummary(livre.getSummary());

        //Afectation Auteur
        livreModel.setAuthor(new AuteurModel());
        livreModel.getAuthor().setAuthorId(livre.getAuthor().getAuthorId());
        livreModel.setAuthorId(livre.getAuthor().getAuthorId());

        //Affectation Categorie
        livreModel.setGenre(new CategorieModel());
        livreModel.getGenre().setGenreId(livre.getGenre().getGenreId());
        livreModel.setGenreId(livre.getGenre().getGenreId());

        dataSource.ajouterLivre(livreModel);
        return null;
    }

    @Override
    public Livre modifierLivre(Livre livre) throws IOException {
        LivreModel livreModel = new LivreModel();
        livreModel.setBookId(livre.getBookId());
        livreModel.setIsbn(livre.getIsbn());
        livreModel.setTitle(livre.getTitle());
        livreModel.setEdition(livre.getEdition());
        livreModel.setPublisher(livre.getPublisher());
        livreModel.setYear(livre.getYear());
        livreModel.setPrice(livre.getPrice());
        livreModel.setSummary(livre.getSummary());

        //Afectation Auteur
        livreModel.setAuthor(new AuteurModel());
        livreModel.getAuthor().setAuthorId(livre.getAuthor().getAuthorId());
        livreModel.setAuthorId(livre.getAuthor().getAuthorId());

        //Affectation Categorie
        livreModel.setGenre(new CategorieModel());
        livreModel.getGenre().setGenreId(livre.getGenre().getGenreId());
        livreModel.setGenreId(livre.getGenre().getGenreId());

        dataSource.modifierLivre(livreModel);
        return null;
    }

    @Override
    public Livre getLivre(int id) throws IOException {
        LivreModel model =  dataSource.getLivre(id);
        AuteurModel auteurModel = model.getAuthor();
        Author author= new Author(auteurModel.getAuthorId(), auteurModel.getName(), auteurModel.getEmail(), auteurModel.getPhone(), auteurModel.getWebsite());
        CategorieModel categorieModel = model.getGenre();
        Categorie categorie = new Categorie(categorieModel.getGenreId(), categorieModel.getName());

        Livre livre=  new Livre(
                model.getBookId(),
                model.getIsbn(),
                model.getTitle(),
                model.getSummary(),
                model.getEdition(),
                model.getPublisher(),
                model.getYear(),
                model.getPrice(),
                model.getImage(),
                author,
                categorie
        );
        return livre;
    }
}
