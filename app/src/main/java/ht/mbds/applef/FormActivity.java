package ht.mbds.applef;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import ht.mbds.applef.dto.Livre;
import ht.mbds.applef.repositories.LivreRepository;
import ht.mbds.applef.ui.auteur.CUAuteurFragment;
import ht.mbds.applef.ui.categorie.CUCategorieFragment;
import ht.mbds.applef.ui.livre.CULivreFragment;
import ht.mbds.applef.ui.livre.DetailLivreFragment;
import ht.mbds.applef.ui.paiement.PaiementFragment;
import ht.mbds.applef.ui.register.RegisterFragment;
import ht.mbds.applef.util.BankUtil;

public class FormActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        int id_screen =  getIntent().getIntExtra(BankUtil.KEY_ID, 0);
        String type_screen =  getIntent().getStringExtra(BankUtil.KEY_SCREEN);

        Bundle bundle = new Bundle();
        bundle.putInt(BankUtil.KEY_ID, id_screen);

       if(type_screen!=null){

               switch (type_screen){
                   case BankUtil.KEY_LIVRE : {

                       getSupportFragmentManager().beginTransaction()
                               .setReorderingAllowed(true)
                               .add(R.id.fragment_container_view, CULivreFragment.class, bundle)
                               .commit();
                       break;
                   }

                   case BankUtil.KEY_DETAIL_LIVRE : {

                       System.out.println("GOD in == > "+id_screen);
                       getSupportFragmentManager().beginTransaction()
                               .setReorderingAllowed(true)
                               .add(R.id.fragment_container_view, DetailLivreFragment.class, bundle)
                               .commit();
                       break;
                   }

                   case BankUtil.KEY_AUTEUR : {
                       getSupportFragmentManager().beginTransaction()
                               .setReorderingAllowed(true)
                               .add(R.id.fragment_container_view, CUAuteurFragment.class, bundle)
                               .commit();
                       break;
                   }

                   case BankUtil.KEY_CATEGORIE : {
                       getSupportFragmentManager().beginTransaction()
                               .setReorderingAllowed(true)
                               .add(R.id.fragment_container_view, CUCategorieFragment.class, bundle)
                               .commit();
                       break;
                   }

                   case BankUtil.KEY_PAIEMENT : {

                       double somme =getIntent().getDoubleExtra(BankUtil.KEY_MONTANT_PAIEMENT, 0.0);
                       bundle.putDouble(BankUtil.KEY_MONTANT_PAIEMENT, somme);
                       getSupportFragmentManager().beginTransaction()
                               .setReorderingAllowed(true)
                               .add(R.id.fragment_container_view, PaiementFragment.class, bundle)
                               .commit();
                       break;
                   }

                   case BankUtil.KEY_REGISTER : {
                      getSupportFragmentManager().beginTransaction()
                               .setReorderingAllowed(true)
                               .add(R.id.fragment_container_view, RegisterFragment.class, bundle)
                               .commit();
                       break;
                   }


                   default:{
                       finish();
                   }

               }

           }else{
                 finish();
           }




    }
}